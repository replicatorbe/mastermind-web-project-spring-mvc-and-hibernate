<%-- 
    Document   : listePersonne
    Created on : 18-mai-2012, 11:51:13
    Author     : jerome
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
          <link rel="stylesheet" type="text/css" href="epfc.css" />
        <title>Information sur le joueur ${ personnes.nom }</title>
    </head>
   <body>
        <h1>Information</h1>
        <table>
            <tr>
                <th>Who ? </th>
            </tr>
                <tr>
                    <td><a href='<c:out value="${mavar}"/>'>${ personnes.nom }</a></td>
                </tr>
        </table>
                    
                    <br /> 
                    <table>
                        <tr>
                            <td><strong>Numéro de partie :</strong></td> 
                            <td><strong>Configuration : </strong></td>
                            <td><strong>Gagné? : </strong></td>
                        </tr>
                  <c:forEach items="${ personnes.getPartie() }" var="part">
                <tr>
                  <td>${ part.getPartie() }</td>
                  <td>Ligne : ${ part.getNbrLigne() } Colonne : ${ part.getNbreColonne() } Couleur : ${ part.getNbreCouleur() } <br /></td>
                  <td>${ part.getGagne() }</td>  
                </tr>
            </c:forEach>
                

        </table>
                    <br />
                      <table>
            <tr>
                <td><strong>Stats global :</strong></td>
       
            </tr>
               
                       <tr>
                    <td>Nombre de joueur : ${ personnes.getStats().compteJoueur() }</td>
                     </tr>
                       <tr>
                    <td>Nombre de partie : ${ personnes.getStats().comptePartie() }</td>
                      </tr>
                        <tr>
                    <td>Total de partie gagné (total): ${ personnes.getStats().totalPartieGagner(1) }</td> 
                      </tr>
                        <tr>
                <td><strong>Stats perso : </strong></td>
       
            </tr>
             <tr>
                    <td>Total de partie perdue : ${ personnes.getStats().totalPartiePerdu(personnes.getNom()) }</td>
                     </tr>
                        <tr>
                    <td>Total de partie joué : ${ personnes.getStats().totalPartie(personnes.getNom()) }</td>
                      </tr>
                      
                        <tr>
                    <td>Total de partie abandonnée ou non finie : ${ personnes.getStats().totalPartieAbandonne(personnes.getNom()) } </td>
                      </tr>
                        <tr>
                    <td>Pourcentage de partie gagnée :  ${ personnes.getStats().pctGagner(personnes.getNom()) } % </td>
                      </tr>
                </tr>
        </table>
        <br />
        <a href='liste.htm'>Retour à la liste</a>
    </body>
</html>
