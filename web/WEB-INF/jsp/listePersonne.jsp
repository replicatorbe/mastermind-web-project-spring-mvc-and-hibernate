<%-- 
    Document   : listePersonne
    Created on : 18-mai-2012, 11:51:13
    Author     : jerome
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
             <link rel="stylesheet" type="text/css" href="epfc.css" />
        <title>Liste des joueurs</title>
    </head>
   <body>
        <h1>Liste des joueurs</h1>
        <table>
            <tr>
                <td><strong>Pseudo :</strong></td>
            </tr>
            <c:forEach items="${ personnes }" var="pers">
                <tr>
                  
                    <td><a href='http://localhost:8080/SpringMVC_Annotation/liste.htm?initListe=${ pers.getId() }'>${ pers.nom }</a></td>
                
                </tr>
            </c:forEach>
                
       

        </table>
        
        
        <br />
        <a href='index.htm'>Retour au jeu</a>
    </body>
</html>
