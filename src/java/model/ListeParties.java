
package model;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Component;

/**
 *
 * @author Jérôme Fafchamps
 * @version 4.0
 * 
 */
@Component
public class ListeParties {

    private List<Partie> p;
    private final int NBMAX_PERS = 5;

    public ListeParties() {
 
        p = new ArrayList<Partie>();
    }


    public List<Partie> getPersonnes() {
        return p;
    }

    public void setParties(List<Partie> per) {
        this.p = per;
    }

    public Partie find(int id) {
        for (Partie pe : p) {
            if (pe.getPartie() == id) {
                return pe;
            }
        }
        return null;
    }

    public boolean delete(Partie pers) {
        return p.remove(pers);
    }

    public void save(Partie pers) {
        Partie pe = find(pers.getPartie());
        if (pe == null) {
            p.add(pers);
          
        } else {
          //  p.setNom(pers.getNom());
          //  p.setPrenom(pers.getPrenom());
        }
    }

    public boolean exist(Partie pers) {
        return p.contains(pers);
    }
}
