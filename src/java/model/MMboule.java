
package model;

import java.io.Serializable;

/**
 *
 * @author Jérôme Fafchamps
 * @version 4.0
 * 
 */
public class MMboule implements Serializable {
    
    
	private int couleur;
	private boolean etat;

	public MMboule() {
	}

	public MMboule(int couleur, boolean etat) {
		this.couleur = couleur;
		this.etat = etat;
	}

	public void setCouleur (int couleur) {
		this.couleur=couleur;
	}

	public void setEtat (boolean etat) {
		this.etat=etat;
	}

	public int getCouleur() {
		return this.couleur;
	}

	public boolean getEtat() {
		return this.etat;
	}

    
}
