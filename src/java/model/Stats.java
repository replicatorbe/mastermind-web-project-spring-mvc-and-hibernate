package model;

import java.io.Serializable;
import model.dao.StatsDAO;

/**
 *
 * @author Jérôme Fafchamps
 * @version 4.0
 * 
 */
public class Stats implements Serializable {

   
	private StatsDAO s;
	
	/**
	 * on charge le STATSDAO
	 */
	
	public Stats() {
		s = new StatsDAO();
	}
	
	/**
	 * compte le nombere de joueur
	 * @return int
	 */
	
	public int compteJoueur() {
		return s.compteJoueur();
	}
	
	/**
	 * compte nome nombre de partie
	 * @return  int
	 */
	
	public int comptePartie() {
		return s.comptePartie();
	}
	
	/**
	 * affiche le pourcentage de parties termin�es (global)
	 * @param nom
	 * @return int
	 */
	public int pctTermine() {
		return s.pctTermine();
	}
	

	/**
	 * nombre de partie total qui est gagn� ou perdue
	 * @param resultat
	 * @return int
	 */
	
	public int totalPartieGagner(int resultat) {
		return s.totalPartieGagner(resultat);
	}
	


	/**
	 * compte le nombre total de parties gagn�es d'un pseudo d�fini
	 * selon un pseudo d�fini
	 * @param nom
	 * @return int
	 */
	public int totalPartieGagner(String nom) {
		return s.totalPartieGagner(nom);
	}

	/**
	 * affiche le total de partie perdu d'un pseudo d�fini
	 * @param nom
	 * @return int
	 */
	public int totalPartiePerdu(String nom) {
		return s.totalPartiePerdu(nom);
		//return 0;
	}


	/**
	 * affiche le pourcentage de parties termin�es d'un pseudo d�fini
	 * @param nom
	 * @return int
	 */
	public int pctTermine(String nom) {
		return s.pctTermine(nom);
	}

	/**
	 * affiche le total de parties d'un pseudo d�fini
	 * @param nom
	 * @return int
	 */
	public int totalPartie(String nom) {
		return s.totalPartie(nom);
	}

	/**
	 * affiche le total de parties abandonn�es ou en cours d'un pseudo d�fini
	 * @param nom
	 * @return int
	 */
	public int totalPartieAbandonne(String nom) {
		return s.totalPartieAbandonne(nom);
	}
	
	/**
	 * pourcentage de parties gagn�es d'un pseudo d�fini
	 * @param nom
	 * @return int
	 */
	public int pctGagner(String nom) {
		return s.pctGagner(nom);
	}

	public String scorefinal1() {
		return s.scorefinal1();
	}
	
	public String scorefinal2() {
		return s.scorefinal2();
	}

}
