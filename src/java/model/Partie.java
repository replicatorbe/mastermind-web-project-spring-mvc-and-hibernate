
package model;

import java.io.Serializable;
import java.util.Random;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Transient;
import model.dao.HibernateFactory;
import model.dao.PartieDAO;
import org.hibernate.Session;

/**
 *
 * @author Jérôme Fafchamps
 * @version 4.0
 * 
 */
@Entity
public class Partie implements Serializable {

      @Id
    @GeneratedValue
    private int numeropartie = 0;
    private int nbrecolonne = 4;
    private int nbreligne = 9;
    private int nbrecouleur = 7;
    private int gagne = 0;
    @ManyToOne
    private Joueur j;
    private int ligne = 1;
    private int colonne = 0;
    private int boulerouge = 0;
    private int boulenoir = 0;
    @Transient
    private MMboule solution[];
    @Transient
    private MMboule proposition[];
    @Transient
    private PartieDAO je;


    public Partie() {
        //on crée les boules

        solution = new MMboule[nbrecolonne];
        proposition = new MMboule[nbrecolonne];

        Random r = new Random();
        chargement();
    }

    public Partie(int colonne, int couleur, int ligne) {


        //configuration de la partie
        //par instance du controleur
        this.nbrecolonne = colonne;
        this.nbreligne = ligne;
        this.nbrecouleur = couleur;

        solution = new MMboule[nbrecolonne];
        proposition = new MMboule[nbrecolonne];

        Random r = new Random();
        chargement();

        je = new PartieDAO();
        je.save(this);
 
    }

    public synchronized void chargement() {
        Random r = new Random();
        for (int j = 0; j < nbrecolonne; j++) {
            //on stock une valeur aléatoire que l'ordinateur jouera
            //permet d'arrondir au supérieur
            int valeur = 1 + r.nextInt(nbrecouleur - 1);
            solution[j] = new MMboule(valeur, false);
            proposition[j] = new MMboule(0, false);
        }
       // this.numeropartie = r.nextInt(9999999);
    }

    public String getCode() {
         ligne++;
        String var = null;
        for (int k = 0; k < nbrecolonne; k++) {
            //System.out.print(solution[k].getCouleur());
            var += solution[k].getCouleur() + " ";
        }
        return var;
    }

    public void traitement(String sol) {
        String[] configuration1 = new String[6];
        configuration1 = sol.split("/");


        for (colonne = 0; colonne < nbrecolonne; colonne++) {
            proposition[colonne].setCouleur(Integer.parseInt(configuration1[colonne]));
        }
    }

    public synchronized int verif_boule_blanche() {


        boulenoir = 0;

        for (colonne = 0; colonne < nbrecolonne; colonne++) {

            if (proposition[colonne].getEtat() == false) {

                for (int i = 0; i < nbrecolonne; i++) {
                    //test si la solution n'a pas encore été testé
                    //dans une autre boucle ou celle-ci.				
                    if (this.solution[i].getEtat() == false && proposition[colonne].getEtat() == false) {
                        //si la proposition est la même que la solution
                        if (proposition[colonne].getCouleur() == this.solution[i].getCouleur()) {
                            //on incrente les boules 
                            boulenoir++;
                            //on bloque pour dire qu'on a déjà incrémenté les boules
                            this.solution[i].setEtat(true);
                            proposition[colonne].setEtat(true);

                        }
                    }
                }
            }
        }
        return boulenoir;
    }

    public synchronized int verif_boule_rouge() {


        boulerouge = 0;

        for (colonne = 0; colonne < nbrecolonne; colonne++) {
            //on vérifie si la proposition en cours et égale  la solution en cours.
            if (proposition[colonne].getCouleur() == solution[colonne].getCouleur()) {
                //on incrémente
                boulerouge++;
                solution[colonne].setEtat(true);
                proposition[colonne].setEtat(true);

            }     
           
        }
          
        if (boulerouge==this.nbrecolonne)
            this.gagne=1;
        //on retourne le nombre de boule rouge pour dÃ©tecter si 4 boules = gagnant
        return boulerouge;
    }

    public synchronized void remise_a_zero() {
        //on remet la ligne proposition/solution Ã  0 pour rechercher une nouvelle ligne     
        for (int i = 0; i < nbrecolonne; i++) {
            this.proposition[i].setEtat(false);
            this.solution[i].setEtat(false);
        }
      
        
    }

    public synchronized void setPartie(int code) {
     //  this.numeropartie = code;
    }

    public synchronized int getPartie() {
        return this.numeropartie;
    }
    
     

    public synchronized int getNbrLigne() {
        return nbreligne;
    }

    public synchronized int getNbreColonne() {
        return nbrecolonne;
    }

    public synchronized int getNbreLigne() {
        return nbreligne;
    }

    public synchronized int getNbreCouleur() {
        return nbrecouleur;
    }

    public synchronized int getLigne() {
        return this.ligne;
    }

    public synchronized void setBouleRouge(int boule) {
        this.boulerouge = boule;
    }

    public synchronized int getColonne() {
        return this.colonne;
    }

    public synchronized int getBouleRouge() {
        return boulerouge;
    }

    public synchronized int getBouleNoir() {
        return boulenoir;
    }

    public synchronized int getNbrCouleur() {
        return nbrecouleur;
    }

       public synchronized int getGagne() {
        return gagne;
    }
       
    public synchronized Joueur getJ() {
        return j;
    }

    public synchronized void setJ(Joueur j) {
       System.out.println("ici plante?" + this.numeropartie);
        je = new PartieDAO();
        this.j = j;
        je.update(this);
    }
    
    public synchronized void update() {
         System.out.println("ici plante?" + this.numeropartie);
        je = new PartieDAO();
        je.update(this);
    }
    
}
