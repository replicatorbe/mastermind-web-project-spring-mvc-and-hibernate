
package model;

import java.util.ArrayList;
import java.util.List;
import model.dao.JoueurDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author Jérôme Fafchamps
 * @version 4.0
 * 
 */
@Component
public class ListeJoueur {

    private List<Joueur> p;
    private final int NBMAX_PERS = 5;
     @Autowired
    private JoueurDAO persDao;

    public ListeJoueur() {
 
        p = new ArrayList<Joueur>();
    }


    public List<Joueur> getPersonnes() {
        return p;
    }

    public void setParties(List<Joueur> per) {
        this.p = per;
    }

    public Joueur find(String id) {
        for (Joueur pe : p) {
            if (pe.nom().equals(id)) {
                return pe;
           }
        }
        return null;
    }
    
        public Joueur find(int id) {
        for (Joueur pe : p) {
            if (pe.getId() == id) {
                return pe;
           }
        }
        return null;
    }

    public boolean delete(Joueur pers) {
        return p.remove(pers);
    }

    public void save(Joueur pers) {
        Joueur pe = find(pers.nom());
        if (pe == null) {
            p.add(pers);
          
        } else {
          //  p.setNom(pers.getNom());
          //  p.setPrenom(pers.getPrenom());
        }
    }

    public boolean exist(Joueur pers) {
        return p.contains(pers);
    }
    
       public void chargerListe() {
        p = persDao.selectAll();
    }

}