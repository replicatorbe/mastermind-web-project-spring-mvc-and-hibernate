package model;

import java.io.Serializable;
import model.dao.JoueurDAO;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.exception.ConstraintViolationException;

/**
 *
 * @author Jérôme Fafchamps
 * @version 4.0
 * 
 */
@Entity
public class Joueur implements Serializable {

    @Id
    @GeneratedValue
    private int id;
 
    @Column(unique = true)
    private String nom;
    @OneToMany(mappedBy = "j", cascade = CascadeType.ALL)
    private List<Partie> jeu = new ArrayList<Partie>();

    @Transient
    private JoueurDAO je;
  
    private Stats sa;
  

    public Joueur() {
    }

    public Joueur(String nom) {
        this.nom = nom;
        sa = new Stats();

        System.out.println("Nouveau joueur " + nom);

        try {

            je = new JoueurDAO();
            this.je.save(this);

        } catch (ConstraintViolationException e) {
              System.out.println("On gera la contrainte");
              
               
            je = new JoueurDAO();
            Joueur tj = je.exist(this);
            
            this.nom = tj.nom;
            this.id = tj.id;
            //this.jeu = tj.jeu;
            
        }
    }

    public void newPartie() {

        jeu.add(new Partie());
         
        
         this.jeu().setJ(this);
    }

    public void newPartie(int colonnea, int couleura, int lignea) {
        System.out.println("PARTIE EN COURS DE CREATION!!!!!!!");
        jeu.add(new Partie(colonnea, couleura, lignea));
        this.jeu().setJ(this);
    }

    public Partie jeu() {
       // System.out.println("NUMERO DE PARTIE "+jeu.get(jeu.size() - 1).getPartie());
        return jeu.get(jeu.size() - 1);
    }

    public String nom() {
        return nom;
    }
    
    public String getNom() {
        return nom;
    }

    public int getId() {
        return this.id;
    }
    
    public Stats getStats() {
        
		return sa;
	}
    
     public List<Partie> getPartie() {
        return jeu;
    }
     
     	
}
