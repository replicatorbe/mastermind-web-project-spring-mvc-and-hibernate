package model.dao;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import model.Joueur;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Component;

/**
 *
 * @author Jérôme Fafchamps
 * @version 4.0
 * 
 */
 
@Component
public class JoueurDAO extends DAO<Joueur> {

    private static Session s = null;

 
    public List<Joueur> selectAll() {
      //  return hibernateTemplate.find(" from Personne");
        
        Session session = HibernateFactory.getSession();
        Transaction tx = session.beginTransaction();
       List q = session.createQuery("from Joueur").list();
        tx.commit();
        return q;
        
    }

    public boolean update(Joueur obj) {
        Session session = HibernateFactory.getSession();
        Transaction tx = session.beginTransaction();
        session.saveOrUpdate(obj);
        tx.commit();
        session.close();
        return true;
    }

    public Joueur exist(Joueur je) {
        Session session = HibernateFactory.getSession();
        Transaction tx = session.beginTransaction();
        Query q = session.createQuery("from Joueur where nom= :myTitle");
        q.setString("myTitle", je.nom());
        Joueur e = (Joueur) q.uniqueResult();
        tx.commit();
        return e;
    }

    @Override
    public boolean create(Joueur obj) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean delete(Joueur obj) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Joueur find(int id) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void save(Joueur p) {
        Session session = HibernateFactory.getSession();
        session.beginTransaction();
        session.saveOrUpdate(p);
        session.getTransaction().commit();
        session.close();
    }
}