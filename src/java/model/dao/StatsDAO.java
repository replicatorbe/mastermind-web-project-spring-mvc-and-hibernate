package model.dao;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.springframework.stereotype.Component;

/**
 *
 * @author Jérôme Fafchamps
 * @version 4.0
 *
 */
@Component
public class StatsDAO implements Serializable {

    /**
     * compte le nombre de joueur
     *
     * @return
     */
    public int compteJoueur() {


        Session session = HibernateFactory.getSession();
        session.beginTransaction();
        Object uniqueResult = session.createQuery("select count(*) from Joueur").uniqueResult();
        session.getTransaction().commit();

        if (uniqueResult == null) {
            return 0;
        }
        System.out.println("COCOCO " + Integer.parseInt(uniqueResult.toString()));
        return Integer.parseInt(uniqueResult.toString());
    }

    /**
     * compte le nombre de partie
     *
     * @return
     */
    public int comptePartie() {

        Session session = HibernateFactory.getSession();
        session.beginTransaction();
        Object uniqueResult = session.createQuery("select count(*) from Partie").uniqueResult();
        session.getTransaction().commit();

        if (uniqueResult == null) {
            return 0;
        }
        return Integer.parseInt(uniqueResult.toString());
    }

    /**
     * total parti gagner ou perdu selon le parametre passer en r�sultat!
     *
     * @param gagne
     * @return
     */
    public int totalPartieGagner(int gagne) {
        String queryString = "SELECT count(*) from Partie WHERE gagne=" + gagne;
        Session session = HibernateFactory.getSession();
        session.beginTransaction();
        Object uniqueResult = session.createQuery(queryString).uniqueResult();
        session.getTransaction().commit();

        if (uniqueResult == null) {
            return 0;
        }

        return Integer.parseInt(uniqueResult.toString());
    }

    //mis
    public int totalPartieGagner(String nom) {
        String queryString = "SELECT count(*) from Partie,Joueur where partie.j_id=Joueur.id and nom = '" + nom + "' and gagne = 1";
        Session session = HibernateFactory.getSession();
        session.beginTransaction();
        Object uniqueResult = session.createSQLQuery(queryString).uniqueResult();
        session.getTransaction().commit();

        if (uniqueResult == null) {
            return 0;
        }

        return Integer.parseInt(uniqueResult.toString());
    }

    //mis
    public int totalPartiePerdu(String nom) {
        String queryString = "SELECT count(*) from Partie partie, Joueur joueur where partie.j_id=joueur.id and nom = '" + nom + "' and gagne = 0";
        Session session = HibernateFactory.getSession();
        session.beginTransaction();
        Object uniqueResult = session.createSQLQuery(queryString).uniqueResult();
        session.getTransaction().commit();

        if (uniqueResult == null) {
            return 0;
        }
        return Integer.parseInt(uniqueResult.toString());
    }

    public int pctTermine(String nom) {
        String queryString = "SELECT count(*) from partie, joueur where partie.j_id=joueur.id and nom = '" + nom + "' and gagne in (1,2)";

        int a = 0, b = 0;
        double c = 0.0;

        Session session = HibernateFactory.getSession();
        session.beginTransaction();
        Object uniqueResult = session.createSQLQuery(queryString).uniqueResult();
        session.getTransaction().commit();

        if (uniqueResult == null) {
            a = 0;
        } else {
            a = Integer.parseInt(uniqueResult.toString());
        }

        queryString = "SELECT count(*) from partie, joueur where partie.j_id=joueur.id and nom = '" + nom + "'";

        Session session1 = HibernateFactory.getSession();
        session1.beginTransaction();
        Object uniqueResult1 = session1.createSQLQuery(queryString).uniqueResult();
        session1.getTransaction().commit();

        if (uniqueResult1 == null) {
            b = 0;
        } else {
            b = Integer.parseInt(uniqueResult1.toString());
        }

        System.out.println("a" + a + " b" + b);

        if (a != 0) {
            c = (double) a / b * 100;
        }

        System.out.println("c" + c);
        return (int) c;
    }

    public int pctTermine() {
        String queryString = "SELECT count(*) from partie where gagne in (1,2)";

        int a = 0, b = 0;
        double c = 0.0;

        Session session = HibernateFactory.getSession();
        session.beginTransaction();
        Object uniqueResult = session.createSQLQuery(queryString).uniqueResult();
        session.getTransaction().commit();

        if (uniqueResult == null) {
            a = 0;
        } else {
            a = Integer.parseInt(uniqueResult.toString());
        }

        queryString = "SELECT count(*) from partie";
        Session session1 = HibernateFactory.getSession();
        session1.beginTransaction();
        Object uniqueResult1 = session1.createSQLQuery(queryString).uniqueResult();
        session1.getTransaction().commit();

        if (uniqueResult1 == null) {
            b = 0;
        } else {
            b = Integer.parseInt(uniqueResult1.toString());
        }
        if (a != 0) {
            c = (double) a / b * 100;
        }
        return (int) c;
    }

    //mis
    public int totalPartie(String nom) {
        String queryString = "SELECT count(*) from partie,joueur WHERE partie.j_id=joueur.id and joueur.nom = '" + nom + "'";
        Session session = HibernateFactory.getSession();
        session.beginTransaction();
        Object uniqueResult = session.createSQLQuery(queryString).uniqueResult();
        session.getTransaction().commit();


        return Integer.parseInt(uniqueResult.toString());
    }

    public int totalPartieAbandonne(String nom) {
        String queryString = "SELECT count(*) from partie,joueur WHERE partie.j_id=joueur.id and joueur.nom = '" + nom + "' and gagne = 0";
        Session session = HibernateFactory.getSession();
        session.beginTransaction();
        Object uniqueResult = session.createSQLQuery(queryString).uniqueResult();
        session.getTransaction().commit();
        return Integer.parseInt(uniqueResult.toString());
    }
//mis

    public int pctGagner(String nom) {

        String queryString = "SELECT count(*) from partie,joueur WHERE partie.j_id=joueur.id and joueur.nom = '" + nom + "' and gagne = 1";

        int a = 0, b = 0;
        double c = 0.0;
        Session session = HibernateFactory.getSession();
        session.beginTransaction();
        Object uniqueResult = session.createSQLQuery(queryString).uniqueResult();
        session.getTransaction().commit();
        a = Integer.parseInt(uniqueResult.toString());


        queryString = "SELECT count(*) from partie,joueur WHERE partie.j_id=joueur.id and joueur.nom = '" + nom + "'";
        Session session1 = HibernateFactory.getSession();
        session1.beginTransaction();
        Object uniqueResult1 = session1.createSQLQuery(queryString).uniqueResult();
        session1.getTransaction().commit();
        b = Integer.parseInt(uniqueResult1.toString());

        if (a != 0) {
            c = (double) a / b * 100;
        }
        return (int) c;
    }

    public String scorefinal1() {
        String queryString = "select nom,abs(((temp*20)-round((nbrecolonne*nbrecouleur*10)-(nbreligne*10)))-10000) sc from partie, config,joueur where partie.confid=config.idconfig and joueur.id=partie.j_id and partie.gagne = 2 /*group by temp DESC*/ order by 2 desc LIMIT 5";
        String pseudo = "";
        String score = "";

        Session session1 = HibernateFactory.getSession();
        session1.beginTransaction();
        List data = session1.createSQLQuery(queryString)
                .setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP)
                .list();
        session1.getTransaction().commit();

        for (Object object : data) {
            Map row = (Map) object;

            pseudo += row.get("nom") + "/";
            score += row.get("sc") + "/";
        }

        return pseudo + score;
    }

    public String scorefinal2() {
        String queryString = "select nom,temp,nbrecolonne,nbrecouleur,nbreligne from partie, config,joueur where partie.confid=config.idconfig and joueur.id=partie.j_id and partie.gagne = 2 order by 2 asc LIMIT 5";
        String pseudo = "";
        String score = "";
        String nombreColonne = "";
        String nombreCouleur = "";
        String nombreLigne = "";


        Session session1 = HibernateFactory.getSession();
        session1.beginTransaction();
        List data = session1.createSQLQuery(queryString)
                .setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP)
                .list();
        session1.getTransaction().commit();

        for (Object object : data) {
            Map row = (Map) object;

            pseudo += row.get("nom") + "/";
            score += row.get("temp") + "/";
            nombreColonne += row.get("nbrecolonne") + "/";
            nombreCouleur += row.get("nbrecouleur") + "/";
            nombreLigne += row.get("nbreligne") + "/";
        }

        return pseudo + score + nombreColonne + nombreCouleur + nombreLigne;
    }
}
