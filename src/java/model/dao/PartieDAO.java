package model.dao;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import model.Partie;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Jérôme Fafchamps
 * @version 4.0
 * 
 */

public class PartieDAO extends DAO<Partie> {

	public PartieDAO() {
		
	}

	public boolean delete(Partie obj) {
		// TODO Auto-generated method stub
		return false;
	}

	public static String now() {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return sdf.format(cal.getTime());

	}


	public boolean update(Partie obj) {
            
	Session session = HibernateFactory.getSession();
        Transaction tx = session.beginTransaction();
        session.saveOrUpdate(obj);
        tx.commit();
        session.close();
        return true;
	}

 
    @Override
    public boolean create(Partie obj) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Partie find(int id) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
    public void save(Partie p) {
         Session session = HibernateFactory.getSession();
        session.beginTransaction();
        session.save(p);
        session.getTransaction().commit();
        session.close(); 
    }

}
