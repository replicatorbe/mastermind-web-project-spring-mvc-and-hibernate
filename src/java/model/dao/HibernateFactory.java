
package model.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

/**
 *
 * @author Jérôme Fafchamps
 * @version 4.0
 * 
 */
public class HibernateFactory {
  
    private static SessionFactory sessionFactory= new AnnotationConfiguration().configure().buildSessionFactory();

    private HibernateFactory() {
    }    
    
    public static Session getSession() {
        return sessionFactory.openSession();
    }
}

