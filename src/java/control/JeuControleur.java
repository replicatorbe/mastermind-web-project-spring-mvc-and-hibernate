
package control;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import model.Joueur;
import model.ListeJoueur;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

/**
 *
 * @author Jérôme Fafchamps
 * @version 4.0
 * 
 */
@Controller
@RequestMapping("/jeu.htm")
@SessionAttributes("p")
public class JeuControleur {

    private ListeJoueur listejoueur;

    @Autowired
    public void setLstjou(ListeJoueur lstpers) {
        this.listejoueur = lstpers;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String setupForm(HttpServletRequest req, @RequestParam(required = false, value = "nom") String nom, ModelMap model) {
        String vue = "";
        Joueur p;
        HttpSession session = req.getSession();

        if (nom == null) {

            if ((req.getParameter("colonne") != null) && (req.getParameter("ligne") != null) && (req.getParameter("couleur") != null)) {

                int colonnea = Integer.parseInt(req.getParameter("colonne"));
                int lignea = Integer.parseInt(req.getParameter("ligne"));
                int couleura = Integer.parseInt(req.getParameter("couleur"));

                p = new Joueur(req.getParameter("nom1"));
                p.newPartie(colonnea, couleura, lignea);
                session.setAttribute("joueur", p);
                listejoueur.save(p);
                vue = "newnumeropartie";
                System.out.println("creation de la partie (avec config)" + p.jeu().getPartie());

            } else {
                p = new Joueur(req.getParameter("nom1"));
                p.newPartie();
                session.setAttribute("joueur", p);
                listejoueur.save(p);
                vue = "newnumeropartie";
                System.out.println("creation de la partie (config par defaut)" + p.jeu().getPartie());
            }
        } else {


            p = listejoueur.find(nom);
       
            //envoi requete
            if (req.getParameter("code") != null) {

                p.jeu().traitement(req.getParameter("code"));
              /**  System.out.println("ENVOI " + req.getParameter("code"));
                System.out.println("CODE " + p.jeu().getCode());
                System.out.println("NBRE DE BOULE ROUGE" + p.jeu().verif_boule_rouge());
                System.out.println("NBRE DE BOULE BLANCHE" + p.jeu().verif_boule_blanche()); **/
                p.jeu().remise_a_zero();
                //connaitre nombre de boule rouge
            } else if (req.getParameter("boulerouge") != null) {
                vue = "boulerouge";

                p.jeu().traitement(req.getParameter("boulerouge"));
             

                p.jeu().update();
                p.jeu().remise_a_zero();
                if (p.jeu().verif_boule_rouge() == p.jeu().getNbreColonne()) {
                    p.jeu().chargement();
                }

            } else if (req.getParameter("bouleblanche") != null) {
                vue = "bouleblanche";

                p.jeu().traitement(req.getParameter("bouleblanche"));
                p.jeu().verif_boule_rouge();
                
               p.jeu().update();
                p.jeu().remise_a_zero();
               

                //si le client ferme la page du jeu
            } else if (req.getParameter("findepartie") != null) {
                vue = "succes";
                listejoueur.delete(p);
              
            }
        }
        model.addAttribute("joueur", p);
        return vue;
    }

  
}