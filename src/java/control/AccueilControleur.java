
package control;

import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

/**
 *
 * @author Jérôme Fafchamps
 * @version 4.0
 * 
 */
@Controller
@RequestMapping("/index.htm")
@SessionAttributes("p")
public class AccueilControleur {

    @RequestMapping(method = RequestMethod.GET)
    public String setupForm(HttpServletRequest req, @RequestParam(required = false, value = "numeropartie") String numeropartie, ModelMap model) {
        String vue = "";
        vue = "accueil";
        return vue;
    }
}