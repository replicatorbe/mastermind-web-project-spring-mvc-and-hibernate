
package control;

import model.ListeJoueur;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Jérôme Fafchamps
 * @version 4.0
 * 
 */
@Controller
public class ListControleur {
    
    private ListeJoueur lstjoueur; 
    
     @Autowired
    public void setLstpers(ListeJoueur lstpers) {
        this.lstjoueur = lstpers;
    }
     
     public ListControleur() {
         
     }
     
      @RequestMapping(value="/liste.htm")
    public ModelAndView menuListe(@RequestParam(value="initListe",required=false) String init) {
        if (init == null) {
            lstjoueur.chargerListe();
            return new ModelAndView("listePersonne","personnes",lstjoueur.getPersonnes());
        } else {
            
         lstjoueur.chargerListe();
             return new ModelAndView("infoJeu","personnes",lstjoueur.find(Integer.parseInt(init)));
        }
    
    }
      
      
    
}
